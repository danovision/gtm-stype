/* global PSUK_sType_Options */
var PSUK_sType = (function () {

    var strIgnoreValue = 'PSUK_ignore';

    // program starts
    function init() {
        parseQueryString();
        appendSource();
    }


    /**
     * is there a PSUK_sType_Options.cookieName cookie with a value?
     * AND
     * is there an anchor on the page that matches PSUK_sType_Options.applyLinkSelector?
     * if so append sType to the anchor's querystring
     */
    function appendSource() {
        var arrApplyLinkNodes = null;

        // if there's no apply links then stop processing
        arrApplyLinkNodes = document.querySelectorAll(PSUK_sType_Options.applyLinkSelector);
        if (arrApplyLinkNodes.length === 0) {
            return;
        }

        // NodeList.forEach does not work in IE
        var len = arrApplyLinkNodes.length;
        var i;
        for (i = 0; i < len; i = i + 1) {
            arrApplyLinkNodes[i].href = addSourceToURL(arrApplyLinkNodes[i].href);
        }
    }

    function addSourceToURL(strRawURL) {
        var queryIndex,
            strQuery,
            strURLBase,
            fragIndex,
            strFrag,
            strRetVal
            = null;


        // need to support IE11 so can't use the URL constructor.
        // we'll deconstruct the raw URL and then build it back up again for output


        // strip away components of the URL starting from the end
        strURLBase = strRawURL;

        // save then strip away hash fragment identifier
        strFrag = '';
        fragIndex = strURLBase.indexOf('#');
        if (fragIndex > -1) {
            strFrag = strURLBase.substring(fragIndex);
            strURLBase = strURLBase.substring(0, fragIndex);
        }

        // does the url already have a (non-empty) querystring?
        // if not we'll make an empty one
        queryIndex = strURLBase.indexOf('?');
        if (queryIndex > -1) {
            strQuery = strURLBase.substring(queryIndex);
            strURLBase = strURLBase.substring(0, queryIndex);
        } else {
            strQuery = '?';
        }
        strQuery = addSourceToQueryString(strQuery);

        strRetVal = strURLBase + strQuery + strFrag;

        return strRetVal;
    }

    
    /**
     * Does the passed query string already have an sType parameter?
     * If so, replace that value with the cookie's value,
     * otherwise add the sType parameter and cookie value to the query string.
     */
    function addSourceToQueryString(strQuery) {
        var objQuery,
            strSource,
            strRetVal,
            arrQueryKeys
            = null;

        // 1. turn strQuery into an object
        objQuery = queryStringToObject(strQuery);

        // 2. set the sTypeParameterName key value
        strSource = readCookie(PSUK_sType_Options.cookieName);
        if (strSource === null) {
            strSource = 'PSUK_Website';
        }
        objQuery[PSUK_sType_Options.sTypeParameterName] = strSource;

        // 3. turn the object back into a query string
        strRetVal = '?';
        arrQueryKeys = Object.getOwnPropertyNames(objQuery);
        arrQueryKeys.forEach(function (thisKey, i) {
            if (i > 0) {
                strRetVal = strRetVal + PSUK_sType_Options.queryStringDelimiter;
            }

            strRetVal = strRetVal + thisKey;
            if (objQuery[thisKey] !== strIgnoreValue) {
                strRetVal = strRetVal + '=' + objQuery[thisKey];
            }
        });

        return strRetVal;
    }


    /**
     * does the URL have a querystring value of param PSUK_sType_Options.sTypeParameterName?
     * if so write this value to the cookie PSUK_sType_Options.cookieName
     */
    function parseQueryString() {
        var strQuery,
            objQuery,
            strSource,
            normalised_sTypeParameterName
            = null;

        // document.location.search works IE11+, result is a string starting with "?"
        strQuery = document.location.search.toLowerCase(); // normalise case

        objQuery = queryStringToObject(strQuery);
        
        // get the media source value from the object and set/update the cookie
        normalised_sTypeParameterName = PSUK_sType_Options.sTypeParameterName.toLowerCase();
        if (objQuery[normalised_sTypeParameterName] && objQuery[normalised_sTypeParameterName].length > 0) {

            strSource = objQuery[normalised_sTypeParameterName];
    
            // set cookie
            createCookie(
                PSUK_sType_Options.cookieName,
                strSource,
                PSUK_sType_Options.cookieExpireAfterDays
            );
        }
    }

    function queryStringToObject(strQuery){
        var objQuery = {},
            arrQuery = null;

        if (strQuery.length === 0 || strQuery === '?') {
            return objQuery;
        }

        if (strQuery[0] === '?') {
            strQuery = strQuery.substring(1);
        }

        // make array of key=value pairs
        arrQuery = strQuery.split(PSUK_sType_Options.queryStringDelimiter);

        // make an object of the query string
        arrQuery.forEach(function (element) {
            // split only on first '=' in a value
            // eg failing case: ?SID=am9ibGlzdF92aYzBkZWQwYTI2ZTU1OTc4MWY5YThhYzQ0MA==&b=2
            // use String.indexOf instead of String.split
            var intParamDelim = element.indexOf('=');

            var strKeyName;
            if (intParamDelim === -1) {
                strKeyName = element;
            } else {
                strKeyName = element.substring(0, intParamDelim);
            }

            var strKeyValue;
            if (intParamDelim === -1) {
                strKeyValue = strIgnoreValue;
            } else if (intParamDelim === element.length - 1) {
                // `element` ends with first `=` in string
                strKeyValue = '';
            } else {
                strKeyValue = element.substring(intParamDelim + 1);
            }

            objQuery[strKeyName] = strKeyValue;
        });

        return objQuery;
    }

    // cookie functions from https://www.quirksmode.org/js/cookies.html
    function createCookie(name,value,days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime()+(days*24*60*60*1000));
            expires = '; expires='+date.toGMTString();
        }
        else expires = '';
        document.cookie = name+'='+value+expires+'; path=/';
    }
    
    function readCookie(name) {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    }

    return {
        init: init
    };

})();

PSUK_sType.init();