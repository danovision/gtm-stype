var PSUK_sType_Options = {
    applyLinkSelector    : '.cta-button > a',
    cookieValueDefault   : 'PSUK_Website',
    sTypeParameterName   : 'sType',
    cookieName           : 'PSUK_sType',
    cookieExpireAfterDays: 30,
    queryStringDelimiter : '&'
};

