const fs = require('fs');
const Terser = require('terser');

const srcPath             = './src';
const distPath            = './dist';
const buildPath           = './build';
const strDistScriptPath   = `${buildPath}/gtm-stype.min.js`;
const strDistTemplatePath = `${distPath}/template.txt`;

let strTemplate     = fs.readFileSync(`${srcPath}/template.txt`, 'utf8');
let strOptions      = fs.readFileSync(`${srcPath}/gtm-stype-options.js`, 'utf8');
let objMinScript    = Terser.minify(fs.readFileSync(`${srcPath}/gtm-stype.js`, 'utf8'));
let strDistScript   = `${strOptions}${objMinScript.code}`;
let strDistTemplate = strTemplate.replace('__REPLACE__', strDistScript);

fs.writeFileSync(strDistScriptPath, strDistScript, 'utf8');
fs.writeFileSync(strDistTemplatePath, strDistTemplate, 'utf8');
