<script>
var PSUK_sType_Options = {
    applyLinkSelector    : '.cta-button > a',
    cookieValueDefault   : 'PSUK_Website',
    sTypeParameterName   : 'sType',
    cookieName           : 'PSUK_sType',
    cookieExpireAfterDays: 30,
    queryStringDelimiter : '&'
};

var PSUK_sType=function(){function e(e){var t,r,i,o,s;return s="",(o=(i=e).indexOf("#"))>-1&&(s=i.substring(o),i=i.substring(0,o)),(t=i.indexOf("?"))>-1?(r=i.substring(t),i=i.substring(0,t)):r="?",r=function(e){var t,r,i;t=n(e),null===(r=function(e){for(var n=e+"=",t=document.cookie.split(";"),r=0;r<t.length;r++){for(var i=t[r];" "==i.charAt(0);)i=i.substring(1,i.length);if(0==i.indexOf(n))return i.substring(n.length,i.length)}return null}(PSUK_sType_Options.cookieName))&&(r="PSUK_Website");return t[PSUK_sType_Options.sTypeParameterName]=r,i="?",Object.getOwnPropertyNames(t).forEach((function(e,n){n>0&&(i+=PSUK_sType_Options.queryStringDelimiter),i+=e,"PSUK_ignore"!==t[e]&&(i=i+"="+t[e])})),i}(r),i+r+s}function n(e){var n={};return 0===e.length||"?"===e||("?"===e[0]&&(e=e.substring(1)),e.split(PSUK_sType_Options.queryStringDelimiter).forEach((function(e){var t,r,i=e.indexOf("=");t=-1===i?e:e.substring(0,i),r=-1===i?"PSUK_ignore":i===e.length-1?"":e.substring(i+1),n[t]=r}))),n}return{init:function(){var t,r,i,o;t=document.location.search.toLowerCase(),r=n(t),o=PSUK_sType_Options.sTypeParameterName.toLowerCase(),r[o]&&r[o].length>0&&(i=r[o],function(e,n,t){var r;if(t){var i=new Date;i.setTime(i.getTime()+24*t*60*60*1e3),r="; expires="+i.toGMTString()}else r="";document.cookie=e+"="+n+r+"; path=/"}(PSUK_sType_Options.cookieName,i,PSUK_sType_Options.cookieExpireAfterDays)),function(){var n=null;if(0===(n=document.querySelectorAll(PSUK_sType_Options.applyLinkSelector)).length)return;var t,r=n.length;for(t=0;t<r;t+=1)n[t].href=e(n[t].href)}()}}}();PSUK_sType.init();
</script>