# GTM sType

## How to use for GTM

You'll find the template file to use in the `dist/template.txt` file.

1. Copy the content of the template into a GTM container.

2. Change the values in the PSUK_sType_Options block. 
    Most likely you'll only need to edit 
        `applyLinkSelector` 
        `cookieValueDefault`
        and possibly
        `sTypeParameterName`

3. Save in GTM and test on the live site.


## Customisable properties

`applyLinkSelector`
    Use this to select all anchors that need the sType added.
    It's a CSS selector that can target multiple nodes.
    (It's passed to querySelectorAll calls.)

`cookieValueDefault`
    Used to indicate that this user did not arrive via tagged media.
    Set it to whatever value is best understood by those reporting on stats,
    eg 'ClientName_SiteName'.
    If this script is run on a page with no `sTypeParameterName` in its url
    and there's no cookie `cookieName` saved to the device,
    then any elements matched by `applyLinkSelector` 
    will have this value appended as the `sTypeParameterName` value.

`sTypeParameterName`
    Case-sensitive value of the parameter name that'll be added to apply links.
    Not case-sensitive when matching this string in the current page's url.

`cookieName`
    Name of the cookie that will be saved to users' devices.

`cookieExpireAfterDays`
    The cookie `cookieName` will expire after this number of days.

`queryStringDelimiter`
    Typically '&' is used to separate querystring key-value pairs from one another.
    Change this value if you need to use a different delimiter.
    Applies to querystrings in the page url as well as in the
    querystring appended to matched `applyLinkSelector` elements.



## Notes for developers

### Setup

- Check out the repo.
- `npm install`


### To build

- `npm run build`

This outputs 

- a template file for users in `./dist/template.txt`
- a minified script in `./build/gtm-stype.min.js`


### To test

- Open the file `./test/test-01.html` in a browser.
- Put a querystring at the end. 
    Default options specify a parameter name of 'stype'.
    eg ?stype=mediaSource
- Click on the 'To CTAs' link.
- Inspect the hrefs to make sure the values have populated correctly.
